package pageObjects;

public class SignInPage {
	public final static String email = "//input[@id='usernameOrEmail']";
	public final static String continueButton = "//button[@type='submit']";
	public final static String alert = "//div[@role='alert']";
}
