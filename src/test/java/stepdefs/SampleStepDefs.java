package stepdefs;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import commonControls.Navigator;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.LandingPage;
import pageObjects.SignInPage;


public class SampleStepDefs {
	WebDriver driver = null;
	WebElement element = null;
	// testing comment for integration
	@Before
    public void beforeTest() {
		System.setProperty("webdriver.chrome.driver","C:\\workspace\\com.demo.otn\\src\\drivers\\chromedriver.exe");
		System.out.println(System.getProperty("webdriver.chrome.driver"));
		//Additional data pulls from test data source as necessary.//slight change
		
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
	
	@After
	public void afterTest() {
		driver.quit();
	}

	@Given("^I navigate to a landing page$")
	public void i_navigate_to_login_page() throws Throwable {	
		driver.navigate().to(Navigator.landingPage);
		Thread.sleep(2000);	
		//driver.navigate().to("https://www.w3schools.com/html/html_examples.asp");
		System.out.println("Navigating to landing page");
	}

	@When("^I click the sign in button$")
	public void i_click_the_sign_in_button() throws Throwable {
		Thread.sleep(1000);
		element = driver.findElement(By.xpath(LandingPage.signInNavBar));
		element.click();
	}

	@And("^I enter a wrong value \"(.*)\"$")
	public void i_enter_some_value(String someValue) throws Throwable {
		Thread.sleep(2000);
		element = driver.findElement(By.xpath(SignInPage.email));
		element.sendKeys(someValue);
	}
	
	@And("I click the continue button")
	public void i_click_the_continue_button() throws Throwable {
		Thread.sleep(2000);
		element = driver.findElement(By.xpath(SignInPage.continueButton));
		element.click();
	}

	@Then("^I expect to see an error message$")
	public void i_expect_to_see_an_error_message() throws Throwable {
		Thread.sleep(2000);
		
		if (driver.findElements(By.xpath(SignInPage.alert)).size() == 0) {
			Assert.fail();
		}

		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

}
