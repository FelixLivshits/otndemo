Feature: Defining the demo for OTN

  Scenario: Attempt to sign in with invalid credentials
    Given I navigate to a landing page
    When I click the sign in button
    And I enter a wrong value "someValue@abc.com"
    And I click the continue button
    Then I expect to see an error message
